import React from "react"
import Head from "next/head"
import Icons from "./icons"
import Topbar from "./topbar"
import Header from "./header"
import Footer from "./footer"

function Layout({ children }) {
  return (
    <div>
      <Head>
        <title>Hera Promosyon</title>
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
        <link rel="shortcut icon" type="image/png" href="/favicon.png" />
        <meta name="theme-color" content="#0aa69b" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0" />
      </Head>

      <Icons />
      <Topbar />
      <Header />
      <main>
        { children }
      </main>
      <Footer />
    </div>
  )
}

export default Layout
