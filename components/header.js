import React from 'react'
import Link from 'next/link'

function Header() {
  return (
    <header className="header">
      <div className="container">
        <div className="header__logo">
          <Link href="/">
            <a>
              <img src="/hera-promosyon-logo.jpg" alt="Hera Promosyon" className="header__logo-image" />
            </a>
          </Link>
        </div>

        <div className="header__search">
          <form>
            <label htmlFor="search_term" className="header__search-label">
              <input type="text" id="search_term" className="header__search-input" placeholder="Ürün ismi veya ürün kodu giriniz..." />
              <button className="header__search-button">
                <span className="header__search-button-icon">
                  <svg className="icon">
                    <use xlinkHref="#icon-search"/>
                  </svg>
                </span>
              </button>
            </label>
          </form>
        </div>

        <div className="header__mobile">
          <i className="header__mobile-icon">
            <svg className="icon">
              <use xlinkHref="#icon-phone"/>
            </svg>
          </i>
          <p className="header__mobile-text">Mobilden Hızlıca Ulaşın! <Link href="tel:+905524291646" prefetch={false}><a className="header__mobile-link">+90 552 429 16 46</a></Link></p>
        </div>
      </div>
    </header>
  )
}

export default Header
