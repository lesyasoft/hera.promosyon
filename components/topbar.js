function Topbar() {
  return (
    <section className="header__top">
      <div className="container">
        <p className="header__top-text">Hera Promosyon'a Hoş Geldiniz.</p>
      </div>
    </section>
  )
}

export default Topbar
