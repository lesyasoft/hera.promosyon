import React from 'react'
import Link from "next/link"

function Footer() {
  return (
    <footer className="footer">
      <div className="container">
        <div className="footer__newsletter">
          <h6 className="footer__title">E-bülten</h6>

          <form className="footer__newsletter-form">
            <label htmlFor="email">
              <input type="text" id="email" placeholder="E-posta adresiniz..." className="footer__newsletter-input" />
              <button className="footer__newsletter-button">
                <i className="icon--envelope">
                  <svg className="icon">
                    <use xlinkHref="#icon-envelope"/>
                  </svg>
                </i>
              </button>
            </label>
          </form>
        </div>

        <div className="footer__social">
          <h6 className="footer__title">Takip Et</h6>

          <div className="footer__social-links">
            <Link href="http://www.twitter.com/vrlylcn" prefetch={false}>
              <a className="footer__social-link footer__social-link--twitter">
                <i className="icon--twitter">
                  <svg className="icon">
                    <use xlinkHref="#icon-twitter"/>
                  </svg>
                </i>
              </a>
            </Link>

            <Link href="http://www.facebook.com/vrlylcn" prefetch={false}>
              <a className="footer__social-link footer__social-link--facebook">
                <i className="icon--facebook">
                  <svg className="icon">
                    <use xlinkHref="#icon-facebook"/>
                  </svg>
                </i>
              </a>
            </Link>

            <Link href="http://www.linkedin.com/vrlylcn" prefetch={false}>
              <a className="footer__social-link footer__social-link--linkedin">
                <i className="icon--linkedin">
                  <svg className="icon">
                    <use xlinkHref="#icon-linkedin"/>
                  </svg>
                </i>
              </a>
            </Link>

            <Link href="http://www.youtube.com/vrlylcn" prefetch={false}>
              <a className="footer__social-link footer__social-link--youtube">
                <i className="icon--youtube">
                  <svg className="icon">
                    <use xlinkHref="#icon-youtube"/>
                  </svg>
                </i>
              </a>
            </Link>
          </div>
        </div>

        <div className="footer__copyright">
          <small className="footer__copyright-text">
            Copyright ©2020
            <Link href="https://ucurtmastudyo.com/" prefetch={false}>
              <a target="_blank" className="footer__copyright-link">Uçurtma Stüdyo</a>
            </Link>
            <Link href="http://www.lesyasoft.com" prefetch={false}>
              <a target="_blank" className="footer__copyright-link">Lesya Soft</a>
            </Link>
          </small>
        </div>
      </div>
    </footer>
  )
}

export default Footer
