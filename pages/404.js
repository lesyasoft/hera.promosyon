import React from 'react'
import Head from 'next/head'
import Layout from "../components/layout"

function Custom404() {
  return (
    <Layout>
      <Head>
        <title>Sayfa  Bulunamadı! - Hera Promosyon</title>
        <meta name="robots" content="noindex" />
        <meta name="googlebot" content="noindex" />
        <meta name="googlebot-news" content="noindex" />
        <meta name="googlebot" content="noindex" />
        <meta name="googlebot-news" content="nosnippet" />
      </Head>

      <div className="container error-page">
        <h1 className="error-page__title">Sayfa Bulunamadı!</h1>
        <p className="error-page__sub-title">Aradığınız sayfa bulunamadı!</p>
        <a href="/" className="button button--secondary">Anasayfa Geri Dön</a>
      </div>
    </Layout>
  )
}

export default Custom404
