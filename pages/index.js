import React from 'react'
import Layout from "../components/layout"
import Link from "next/link"

function Home() {
  return (
    <Layout>
      <div className="container">
        <section className="banners">
          <div className="banner__item">
            <img src="/urun-talepleriniz-icin-profesyonel-destek.jpg" alt="Profesyonel Destek" className="banner__image" />
          </div>

          <div className="banner__item">
            <img src="/hizli-geri-bildirim.jpg" alt="Hızlı Geri Bildirim" className="banner__image" />
          </div>

          <div className="banner__item">
            <img src="/adrese-teslimat.jpg" alt="Adrese Teslimat" className="banner__image" />
          </div>
        </section>

        <section className="banners">
          <div className="banner__item">
            <Link href="/">
              <a>
                <img src="/marka-kalemler.jpg" alt="Marka Kalemler" className="banner__image" />
              </a>
            </Link>
          </div>

          <div className="banner__item">
            <img src="/tasarim-usb-bellekler.jpg" alt="Tasarım USB Bellekler" className="banner__image" />
          </div>

          <div className="banner__item">
            <Link href="/">
              <a>
                <img src="/2019-ozel-ajandalar.jpg" alt="2019 Özel Ajandalar" className="banner__image" />
              </a>
            </Link>
          </div>

          <div className="banner__item">
            <img src="/mi-markali-urunler.jpg" alt="" className="banner__image" />
          </div>
        </section>
      </div>
    </Layout>
  )
}

export default Home
