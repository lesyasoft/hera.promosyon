import '../styles/desktop.css'
import '../styles/mobile.css'

export default function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}
