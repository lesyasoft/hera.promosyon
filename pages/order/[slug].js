import React from 'react'
import Head from "next/head"
import Layout from "../../components/layout"

function Order() {
  return (
    <Layout>
      <Head>
        <title>Order Page</title>
      </Head>

      <h1>Order Page</h1>
    </Layout>
  )
}

export default Order
