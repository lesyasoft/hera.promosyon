import React from 'react'
import Head from 'next/head'
import Layout from "../../components/layout"

function Category() {
  return (
    <Layout>
      <Head>
        <title>Category Page</title>
      </Head>

      <h1>Category Page</h1>
    </Layout>
  )
}

export default Category
