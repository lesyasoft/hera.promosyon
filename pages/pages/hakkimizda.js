import React from 'react'
import Head from "next/head"
import Layout from "../../components/layout"
import Link  from 'next/link'

function About() {
  return (
    <Layout>
      <Head>
        <title>Hakkımızda - Hera Promosyon</title>
      </Head>

      <div className="container">
        <div className="breadcrumb">
          <Link href="/">
            <a className="breadcrumb__link">
              <i className="icon--home">
                <svg className="icon">
                  <use xlinkHref="#icon-home"/>
                </svg>
              </i>
              ANASAYFA
            </a>
          </Link>

          <span className="icon--seperator">
            <svg className="icon">
                <use xlinkHref="#icon-arrow-right"/>
              </svg>
          </span>

         <p className="breadcrumb__link breadcrumb__link--active">HAKKIMIZDA</p>
        </div>

        <div className="flat-page__title">
          <h1 className="flat-page__title-text"><span>HERA PROMOSYON'A</span> HOŞ GELDİNİZ!</h1>
        </div>

        <div className="flat-page__content flat-page__content--two-column">
          <div className="flat-page__image">
            <img src="/hakkimizda.jpg" alt="Hera Promosyon"/>
          </div>

          <div className="flat-page__text">
            <p>Farklı olmak ve öne çıkmak zor bunu biliyoruz. Tıpkı bunu nasıl başaracığınızı bildiğimiz gibi.</p>
            <p>Markanızı daha tanınır hale getirmek için buradayız. Sizin için hazırladığımız promosyonel ürünler ve hediyeler, firmanıza olan bağlılığı arttıracak ve sizi daha akılda kalıcı kılacak.</p>
            <p>Sizi ve işinizi yansıtacak yüzlerce çeşit ürünle karşınızdayız. Tüm ahşap, plastik, tekstil ve basılı ürünlerimiz sizin için özel olarak tasarlanır. Bu süreçte sizler için yeni trend ve tasarım ürünler geliştirmek bizim işimiz.</p>
            <p>Bu portföy size ilham verebilmek ve kendimizi tanıtabilmek için hazırlandı. Portföydeki ürünlerimizle ilgili soru sormak veya portföy dışında talep edeceğiniz ürünlerle ilgili danışmak için bizimle irtibata geçebilirsiniz.</p>
            <p><i>Keşfedin ve keyfini çıkarın..</i></p>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default About
