import React from 'react'
import Head from "next/head"
import Layout from "../components/layout"

function Search() {
  return (
    <Layout>
      <Head>
        <title>Search Page</title>
      </Head>

      <h1>Search Page</h1>
    </Layout>
  )
}

export default Search
