import React from 'react'
import Head from "next/head"
import Layout from "../../components/layout"

function Product() {
  return (
    <Layout>
      <Head>
        <title>Product Page</title>
      </Head>

      <h1>Product Page</h1>
    </Layout>
  )
}

export default Product
