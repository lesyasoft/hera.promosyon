<?php

define('DB_URI_LIST_TABLE', 'uri_list');
define('DB_BANNERS_TABLE', 'banners');
define('DB_CATEGORIES_TABLE', 'categories');
define('DB_CONTACT_MESSAGES_TABLE', 'contact_messages');
define('DB_NAVIGATION_TABLE', 'navigation');
define('DB_NEWSLETTER_USERS_TABLE', 'newsletter_users');
define('DB_USERS_TABLE', 'users');
define('DB_ORDERS_TABLE', 'orders');
define('DB_PAGES_TABLE', 'pages');
define('DB_PRODUCTS_TABLE', 'products');
define('DB_PRODUCT_IMAGES_TABLE', 'product_images');
define('DB_SETTINGS_TABLE', 'settings');
define('DB_WIDGETS_TABLE', 'widgets');
define('DB_WIDGET_CONTENTS_TABLE', 'widget_contents');
define('DB_ERROR_PAGES_TABLE', 'error_pages');